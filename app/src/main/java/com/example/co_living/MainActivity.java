package com.example.co_living;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.co_living.Adapter.ListCityAdapter;
import com.example.co_living.Adapter.ListCoworkingAdapter;
import com.example.co_living.Authentication.SignInActivity;
import com.example.co_living.Model.City;
import com.example.co_living.Model.Coworking;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
  private FirebaseAuth mAuth;
  private FirebaseAuth.AuthStateListener mAuthhListener;
  private DatabaseReference cityReference;
  private DatabaseReference coworkingReference;
  private ArrayList<City> cities = new ArrayList<>();
  private ArrayList<Coworking> coworkings = new ArrayList<>();

  ProgressBar progressBar;
  TextView sign_out;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

//    cities      = new ArrayList<>();
//    coworkings  = new ArrayList<>();
    getSupportActionBar().hide();
    mAuth       = FirebaseAuth.getInstance();

    progressBar = findViewById(R.id.progress_bar);
    sign_out = findViewById(R.id.sign_out);

    mAuthhListener = getAuthStateListener();

    if (progressBar != null) {
      progressBar.setVisibility(View.GONE);
    }

    sign_out.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        MainActivity.this.startActivity(new Intent(MainActivity.this, SignInActivity.class));
        mAuth.signOut();
      }
    });


  }


  @Override
  protected void onResume() {
    super.onResume();
    progressBar.setVisibility(View.GONE);
  }

  @Override
  protected void onStart() {
    super.onStart();
    coworkingReference = FirebaseDatabase.getInstance().getReference("coworking");
    cityReference      = FirebaseDatabase.getInstance().getReference("city");


    mAuth.addAuthStateListener(mAuthhListener);

    cityReference.addValueEventListener(new ValueEventListener() {
      @Override
      public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        cities.clear();
        for (DataSnapshot citySnapshot : dataSnapshot.getChildren()){
          City city = citySnapshot.getValue(City.class);
          cities.add(city);
        }
        initCitiesView();
      }

      @Override
      public void onCancelled(@NonNull DatabaseError databaseError) {
        Log.w("DATABASE", "loadPost:onCancelled", databaseError.toException());
      }
    });

    coworkingReference.addValueEventListener(new ValueEventListener() {
      @Override
      public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        coworkings.clear();
        for(DataSnapshot coworkingSnapshot : dataSnapshot.getChildren()){
          Coworking coworking = coworkingSnapshot.getValue(Coworking.class);
          coworkings.add(coworking);
        }

        initCoworkingView();
      }

      @Override
      public void onCancelled(@NonNull DatabaseError databaseError) {
        Log.w("DATABASE", "loadPost:onCancelled", databaseError.toException());
      }
    });
  }

  @Override
  protected void onStop() {
    super.onStop();
    if (mAuthhListener != null) {
      mAuth.removeAuthStateListener(mAuthhListener);
    }
  }

  private FirebaseAuth.AuthStateListener getAuthStateListener(){
    return new FirebaseAuth.AuthStateListener() {
      @Override
      public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        FirebaseUser user_active = mAuth.getCurrentUser();

        System.out.println();

        if (user_active == null) {
          MainActivity.this.startActivity(new Intent(MainActivity.this, SignInActivity.class));
          MainActivity.this.finish();
        }
      }
    };
  }

  private void initCitiesView(){
    RecyclerView cityRecyclerView;
    ListCityAdapter cityAdapter = new ListCityAdapter(this, cities);

    cityRecyclerView = findViewById(R.id.rv_city);
    cityRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
    cityRecyclerView.setHasFixedSize(true);
    cityRecyclerView.setAdapter(cityAdapter);
  }

  private void initCoworkingView(){
    RecyclerView recyclerView;
    ListCoworkingAdapter coworkingAdapter = new ListCoworkingAdapter(this, "HORIZONTAL" , coworkings);

    recyclerView = findViewById(R.id.rv_coworking);
    recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
    recyclerView.setHasFixedSize(true);
    recyclerView.setAdapter(coworkingAdapter);
  }

  @Override
  public void onClick(View view) {
    switch (view.getId()){
      case R.id.cekPesanan :
        Intent cekPesanan = new Intent(this, PesananActivity.class);
        startActivity(cekPesanan);
        System.out.println("oke oke");
    }
  }
}
