package com.example.co_living;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.co_living.Adapter.ListCoworkingAdapter;
import com.example.co_living.Adapter.OrderAdapter;
import com.example.co_living.Model.Order;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class PesananActivity extends AppCompatActivity {
  ArrayList<Order> orders;
  DatabaseReference orderRef;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_pesanan);

    orders = new ArrayList<>();
  }

  @Override
  protected void onStart() {
    super.onStart();

    orderRef = FirebaseDatabase.getInstance().getReference("orders");

    orderRef.addValueEventListener(new ValueEventListener() {
      @Override
      public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        orders.clear();
        for(DataSnapshot orderData : dataSnapshot.getChildren()){
          Order order = orderData.getValue(Order.class);

          orders.add(order);
        }

        initView();
      }

      @Override
      public void onCancelled(@NonNull DatabaseError databaseError) {

      }
    });
  }

  private void initView(){
    RecyclerView recyclerView;
    OrderAdapter orderAdapter = new OrderAdapter( orders, this);

    recyclerView = findViewById(R.id.rv_order);
    recyclerView.setLayoutManager(new LinearLayoutManager(this));
    recyclerView.setAdapter(orderAdapter);
  }
}
