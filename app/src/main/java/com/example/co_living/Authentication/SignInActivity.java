package com.example.co_living.Authentication;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.co_living.MainActivity;
import com.example.co_living.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class SignInActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;

    EditText input_email, input_password;
    Button btn_SignUp, btn_SignIn;
    TextView tv_LupaPassword;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAuth = FirebaseAuth.getInstance();

        if (mAuth.getCurrentUser() != null) {
            // User is logged in
            startActivity(new Intent(SignInActivity.this, MainActivity.class));
            finish();
        }

        setContentView(R.layout.sign_in);

        input_email = findViewById(R.id.input_email);
        input_password = findViewById(R.id.input_password);
        btn_SignUp = findViewById(R.id.btn_SignUp);
        btn_SignIn = findViewById(R.id.btn_SignIn);
        tv_LupaPassword = findViewById(R.id.tv_LupaPassword);
        progressBar = findViewById(R.id.progress_bar);

        btn_SignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SignInActivity.this.startActivity(new Intent(SignInActivity.this, SignUpActivity.class));
                SignInActivity.this.finish();
            }
        });

        tv_LupaPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SignInActivity.this.startActivity(new Intent(SignInActivity.this, LupaPasswordActivity.class));
                SignInActivity.this.finish();
            }
        });

        btn_SignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userEmail = input_email.getText().toString().trim();
                String userPassword = input_password.getText().toString().trim();

                if (TextUtils.isEmpty(userEmail)) {
                    Toast.makeText(SignInActivity.this, "Masukkan email Anda.", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(userPassword)) {
                    Toast.makeText(SignInActivity.this, "Masukkan password Anda.", Toast.LENGTH_SHORT).show();
                }

                progressBar.setVisibility(View.VISIBLE);

                mAuth.signInWithEmailAndPassword(userEmail, userPassword)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                progressBar.setVisibility(View.GONE);
                                if (!task.isSuccessful()) {
                                    Toast.makeText(SignInActivity.this, "Email atau password yang Anda masukkan salah.", Toast.LENGTH_SHORT).show();
                                } else {
                                    SignInActivity.this.startActivity(new Intent(SignInActivity.this, MainActivity.class));
                                    SignInActivity.this.finish();
                                }
                            }
                        });
            }
        });
    }
}
