package com.example.co_living.Authentication;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.co_living.MainActivity;
import com.example.co_living.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class SignUpActivity extends AppCompatActivity {

    private static final String TAG = "SignupActivity";

    private FirebaseAuth mAuth;

    EditText input_email, input_password;
    Button btn_SignUp, btn_SignIn;
    TextView tv_LupaPassword;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up);

        mAuth = FirebaseAuth.getInstance();

        input_email = findViewById(R.id.input_email);
        input_password = findViewById(R.id.input_password);
        btn_SignUp = findViewById(R.id.btn_SignUp);
        btn_SignIn = findViewById(R.id.btn_SignIn);
        tv_LupaPassword = findViewById(R.id.tv_LupaPassword);
        progressBar = findViewById(R.id.progress_bar);

        btn_SignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SignUpActivity.this.startActivity(new Intent(SignUpActivity.this, SignInActivity.class));
                SignUpActivity.this.finish();
            }
        });

        btn_SignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SignUpActivity.this.register_user();
            }
        });
    }

    public void register_user() {
        String userEmail = input_email.getText().toString().trim();
        String userPassword = input_password.getText().toString().trim();

        if (TextUtils.isEmpty(userEmail)) {
            showToast("Masukkan email Anda.");
            return;
        }

        if (TextUtils.isEmpty(userPassword)) {
            showToast("Masukkan password Anda.");
            return;
        }

        progressBar.setVisibility(View.VISIBLE);

        mAuth.createUserWithEmailAndPassword(userEmail, userPassword)
                .addOnCompleteListener(SignUpActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "New user registration: " + task.isSuccessful());
                        if (!task.isSuccessful()) {
                            SignUpActivity.this.showToast("Authentication failed." + task.getException());
                        } else {
                            SignUpActivity.this.startActivity(new Intent(SignUpActivity.this, MainActivity.class));
                            SignUpActivity.this.finish();
                        }
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        progressBar.setVisibility(View.GONE);
    }

    public void showToast(String toasText) {
        Toast.makeText(this, toasText, Toast.LENGTH_LONG).show();
    }
}