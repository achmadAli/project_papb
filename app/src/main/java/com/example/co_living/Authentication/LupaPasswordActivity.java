package com.example.co_living.Authentication;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.co_living.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class LupaPasswordActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    EditText input_email;
    Button btn_reset_password, btn_back;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lupa_password);

        mAuth = FirebaseAuth.getInstance();

        input_email = findViewById(R.id.input_email);
        btn_reset_password = findViewById(R.id.btn_reset_password);
        btn_back = findViewById(R.id.btn_back);
        progressBar = findViewById(R.id.progress_bar);

        btn_reset_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userEmail = input_email.getText().toString().trim();

                if (TextUtils.isEmpty(userEmail)) {
                    Toast.makeText(LupaPasswordActivity.this, "Masukkan Email Anda", Toast.LENGTH_LONG).show();
                    return;
                }

                progressBar.setVisibility(View.VISIBLE);

                mAuth.sendPasswordResetEmail(userEmail)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(LupaPasswordActivity.this, "Informasi reset password telah dikirim ke Email Anda", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(LupaPasswordActivity.this, "Email tersebut tidak terdaftar", Toast.LENGTH_SHORT).show();
                                }

                                progressBar.setVisibility(View.GONE);
                            }
                        });
            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LupaPasswordActivity.this.finish();
            }
        });
    }
}
