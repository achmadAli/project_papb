package com.example.co_living;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.co_living.Model.Coworking;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

public class DetailCoworking extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener, OnMapReadyCallback {
    TextView tvCounter;
    Button btnIncrement;
    Button btnDecrement;

    DatePicker datePicker;

    TextView coName, coAddress, coRate, coDescription;
    ImageView coImage;

    private GoogleMap mMap;

    int counter = 0;
    int mHOUR =0;
    int mMinute=0;
    private String latitude;
    private String longitude;
    private Double lat;
    private Double longg;


    private TextView mlatitude, mlongitude;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_coworking);
        getSupportActionBar().hide();

        Button button_m = (Button)findViewById(R.id.button_mulai);
        final int mCounter = 0;
            button_m.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DialogFragment timePicker = new TimePickerFragment();
                    timePicker.show(getSupportFragmentManager(), "time picker");
                }
            });

        tvCounter = findViewById(R.id.tvCounter);
        btnIncrement = findViewById(R.id.btnIncrement);
        btnDecrement = findViewById(R.id.btnDecrement);

        coName = findViewById(R.id.co_name);
        coDescription = findViewById(R.id.co_description);
        coAddress = findViewById(R.id.co_address);
        coRate = findViewById(R.id.co_rate);
        coImage = findViewById(R.id.co_image);

        datePicker = findViewById(R.id.datePicker);

        Intent intent = getIntent();
        final Coworking coworking = intent.getParcelableExtra("Object");
        coName.setText(coworking.getName());
        coDescription.setText(coworking.getDescription());
        coAddress.setText(coworking.getAddress());
        coRate.setText(coworking.getRate());
        Picasso.get().load(coworking.getImg_path()).into(coImage);



        //on app launch TextView will show zero
        tvCounter.setText("0");

        btnIncrement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter = counter + 1;
                tvCounter.setText(String.valueOf(counter));
            }
        });

        btnDecrement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter = counter - 1;
                tvCounter.setText(String.valueOf(counter));
            }
        });

        //button pesan
        Button btnPesanSekarang = findViewById(R.id.btnPesanSekarang);
        btnPesanSekarang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent moveWithDataIntent = new Intent(v.getContext(), DetailPesanan.class);
                moveWithDataIntent.putExtra(DetailPesanan.HOUR, mHOUR);
                moveWithDataIntent.putExtra(DetailPesanan.MINUTE, mMinute);
                moveWithDataIntent.putExtra(DetailPesanan.DURATION, counter);
                moveWithDataIntent.putExtra("Tanggal", datePicker.getDayOfMonth() + "/" + (datePicker.getMonth()+1) + "/" + datePicker.getYear());
                moveWithDataIntent.putExtra("Object", coworking);
                v.getContext().startActivity(moveWithDataIntent);

            }
        });

        mlatitude = (TextView) findViewById(R.id.mlatitude);
        mlongitude = (TextView) findViewById(R.id.mlongitude);
        mlatitude.setText(coworking.getLatitude());
        mlongitude.setText(coworking.getLongitude());

        lat = Double.parseDouble(coworking.getLatitude());
        longg = Double.parseDouble(coworking.getLongitude());

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
        TextView textView_m = (TextView)findViewById(R.id.tv_mulai);
        textView_m.setText(hourOfDay + ":" + minute);
        mHOUR=hourOfDay;
        mMinute=minute;

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        Intent intent = getIntent();
        final Coworking coworking = intent.getParcelableExtra("Object");
        LatLng sydney = new LatLng(lat, longg);
        mMap.addMarker(new MarkerOptions().position(sydney).title(coworking.getName()));
        float zoomLevel = 15.0f; //This goes up to 21
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, zoomLevel));
    }
}
