package com.example.co_living;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.co_living.Model.Coworking;
import com.example.co_living.Model.Order;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.w3c.dom.Text;

public class DetailPesanan extends AppCompatActivity {
  public Coworking coworking;
  public static final String HOUR = "8";
  public static final String MINUTE = "extra_age";
  public static final String DURATION = "extra_name";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_detail_pesanan);
      getSupportActionBar().hide();

    TextView tvDurasi = findViewById(R.id.durasi);
    TextView tvJamStart = findViewById(R.id.jam_start);
    TextView coName = findViewById(R.id.co_name);
    TextView date = findViewById(R.id.tanggal);

    int hour = getIntent().getIntExtra(HOUR, 8);
    int minute = getIntent().getIntExtra(MINUTE,0);
    int duration = getIntent().getIntExtra(DURATION,0);

    Intent intent = getIntent();
    coworking = intent.getParcelableExtra("Object");

    coName.setText(coworking.getName());
    date.setText(getIntent().getStringExtra("Tanggal"));

    tvJamStart.setText(hour + " : " + minute);
    String durationNumber = duration + " Jam";
    tvDurasi.setText(durationNumber);

    //button konfirm
    Button btnConfirm = findViewById(R.id.btnConfirm);
    btnConfirm.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        String number = "++6282383340083";
        String text = "Halo saya pesan: " + "\n"+ coworking.getName() + "\n" + getIntent().getStringExtra("Tanggal") + "\n";
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("http://api.whatsapp.com/send?phone=" + number + "&text=" + text));
        startActivity(intent);

        saveData(initOrderObj());
      }
    });
  }

  private Order initOrderObj(){
    Order order = new Order(
        FirebaseAuth.getInstance().getCurrentUser().getUid(),
        coworking.getId_coworking(),
        coworking.getName(),
        getIntent().getStringExtra("Tanggal"),
        getIntent().getIntExtra(HOUR, 8) + " : " + getIntent().getIntExtra(MINUTE, 0),
        String.valueOf(getIntent().getIntExtra(DURATION, 0))
    );

    return order;
  }

  public void saveData(Order order){
    DatabaseReference orderRef = FirebaseDatabase.getInstance().getReference("orders");

    String orderId = orderRef.push().getKey();
    orderRef.child(orderId).setValue(order);
  }
}
