package com.example.co_living;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.widget.TextView;

import com.example.co_living.Adapter.ListCoworkingAdapter;
import com.example.co_living.Model.Coworking;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ListCoworking extends AppCompatActivity {
  private DatabaseReference coworkingReference;
  private ArrayList<Coworking> coworkings;
  private String location;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_list_coworking);

    location = getIntent().getStringExtra("location");
    coworkings = new ArrayList<>();
  }

  @Override
  protected void onStart() {
    super.onStart();

    coworkingReference = FirebaseDatabase.getInstance().getReference("coworking");

    coworkingReference.addValueEventListener(new ValueEventListener() {
      @Override
      public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        coworkings.clear();

        for(DataSnapshot coworkingData : dataSnapshot.getChildren()){
          Coworking coworking = coworkingData.getValue(Coworking.class);

          if(coworking.getName().contains(location))
            coworkings.add(coworking);
        }

        initCoworkingView();

        if(coworkings.size() == 0){
          findViewById(R.id.no_result_text).setVisibility(1);
        }
      }

      @Override
      public void onCancelled(@NonNull DatabaseError databaseError) {
        Log.w("DATABASE", "loadPost:onCancelled", databaseError.toException());
      }
    });

  }

  private void initCoworkingView(){
    RecyclerView recyclerView;
    ListCoworkingAdapter coworkingAdapter = new ListCoworkingAdapter(this, "VERTICAL" ,coworkings);

    recyclerView = findViewById(R.id.rv_coworking);
    recyclerView.setLayoutManager(new LinearLayoutManager(this));
    recyclerView.setAdapter(coworkingAdapter);

    LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
    RecyclerView coworkingViews = findViewById(R.id.rv_coworking);
    coworkingViews.setLayoutManager(layoutManager);
  }
}
