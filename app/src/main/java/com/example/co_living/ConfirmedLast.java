package com.example.co_living;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ConfirmedLast extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmed_last);
        getSupportActionBar().hide();

        //button back
        Button btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent moveWithDataIntent = new Intent(ConfirmedLast.this, MainActivity.class);
//                moveWithDataIntent.putExtra(DetailPesanan.HOUR, mHOUR);
//                moveWithDataIntent.putExtra(DetailPesanan.MINUTE, mMinute);
//                moveWithDataIntent.putExtra(DetailPesanan.DURATION, counter);

                startActivity(moveWithDataIntent);
            }
        });
    }
}
