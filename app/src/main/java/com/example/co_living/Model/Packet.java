package com.example.co_living.Model;

public class Packet {
  private String name;
  private int price;

  public Packet() {
  }

  public Packet(String name, int price) {
    this.name = name;
    this.price = price;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }
}
