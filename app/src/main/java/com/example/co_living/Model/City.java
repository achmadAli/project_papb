package com.example.co_living.Model;

public class City {
    private String name;
    private String image;

    public City() {
    }

    public City (String city, String image) {
        this.name = city;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}