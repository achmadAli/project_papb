package com.example.co_living.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.co_living.Maps.DownloadUrl;

import java.util.ArrayList;

public class Coworking implements Parcelable {
  private String name;
  private String id_coworking;
  private String description;
  private String address;
  private String latitude;
  private String longitude;
  private String img_path;
  private String rate;

  public Coworking() {
  }

  public Coworking(String name, String description, String address, String latitude, String longitude, String img_path, String rate, String id_coworking) {
    this.name = name;
    this.description = description;
    this.address = address;
    this.latitude = latitude;
    this.longitude = longitude;
    this.img_path = img_path;
    this.rate = rate;
    this.id_coworking = id_coworking;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  //Constructor
  public Coworking(Parcel parcel) {
    name = parcel.readString();
    description = parcel.readString();
    address = parcel.readString();
    latitude = parcel.readString();
    longitude = parcel.readString();
    img_path = parcel.readString();
    rate = parcel.readString();
    id_coworking = parcel.readString();
  }

  public static final Creator<Coworking> CREATOR = new Creator<Coworking>() {
    @Override
    public Coworking createFromParcel(Parcel parcel) {
      return new Coworking(parcel);
    }

    @Override
    public Coworking[] newArray(int i) {
      return new Coworking[i];
    }
  };

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getLatitude() {
    return latitude;
  }

  public void setLatitude(String latitude) {
    this.latitude = latitude;
  }

  public String getLongitude() {
    return longitude;
  }

  public void setLongitude(String longitude) {
    this.longitude = longitude;
  }

  public String getImg_path() {
    return img_path;
  }

  public void setImg_path(String img_path) {
    this.img_path = img_path;
  }

  public String getRate() {
    return rate;
  }

  public void setRate(String rate) {
    this.rate = rate;
  }

  public String getId_coworking() {
    return id_coworking;
  }

  public void writeToParcel(Parcel parcel, int i){
    parcel.writeString(name);
    parcel.writeString(description);
    parcel.writeString(address);
    parcel.writeString(latitude);
    parcel.writeString(longitude);
    parcel.writeString(img_path);
    parcel.writeString(rate);
    parcel.writeString(id_coworking);
  }

}

