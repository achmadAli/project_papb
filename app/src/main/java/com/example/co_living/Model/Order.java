package com.example.co_living.Model;

public class Order {
  private String uid;
  private String coworking_id;
  private String coworkingName;
  private String date;
  private String startAt;
  private String duration;

  public Order() {
  }

  public Order(String uid, String coworking_id, String coworkingName, String date, String startAt, String duration) {
    this.uid = uid;
    this.coworking_id = coworking_id;
    this.coworkingName = coworkingName;
    this.date = date;
    this.startAt = startAt;
    this.duration = duration;
  }

  public String getUid() {
    return uid;
  }

  public void setUid(String uid) {
    this.uid = uid;
  }

  public String getCoworking_id() {
    return coworking_id;
  }

  public void setCoworking_id(String coworking_id) {
    this.coworking_id = coworking_id;
  }

  public String getCoworkingName() {
    return coworkingName;
  }

  public void setCoworkingName(String coworkingName) {
    this.coworkingName = coworkingName;
  }

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public String getStartAt() {
    return startAt;
  }

  public void setStartAt(String startAt) {
    this.startAt = startAt;
  }

  public String getDuration() {
    return duration;
  }

  public void setDuration(String duration) {
    this.duration = duration;
  }
}
