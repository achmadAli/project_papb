package com.example.co_living.Model;

public class Operational {
  private String day;
  private String close;
  private String open;

  public Operational() {
  }

  public Operational(String day, String close, String open) {
    this.day = day;
    this.close = close;
    this.open = open;
  }

  public String getDay() {
    return day;
  }

  public void setDay(String day) {
    this.day = day;
  }

  public String getClose() {
    return close;
  }

  public void setClose(String close) {
    this.close = close;
  }

  public String getOpen() {
    return open;
  }

  public void setOpen(String open) {
    this.open = open;
  }
}
