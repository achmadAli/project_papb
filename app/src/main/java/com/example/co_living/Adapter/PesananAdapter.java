package com.example.co_living.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.co_living.DetailCoworking;
import com.example.co_living.Model.Coworking;
import com.example.co_living.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PesananAdapter extends RecyclerView.Adapter<PesananAdapter.ListViewHolder>  {
    private ArrayList<Coworking> coworkings;
    private Context context;
    private String mode;


    public PesananAdapter(Context context, String mode, ArrayList<Coworking> coworkings) {
        this.coworkings = coworkings;
        this.context    = context;
        this.mode       = mode;
    }

    @NonNull
    @Override
    public PesananAdapter.ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull PesananAdapter.ListViewHolder holder, int position) {
        final Coworking currentCoworking = coworkings.get(position);

        holder.name.setText(currentCoworking.getName());
        holder.address.setText(currentCoworking.getAddress());
        Picasso.get().load(currentCoworking.getImg_path()).into(holder.image);

        holder.image.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                Intent moveWithDataIntent = new Intent(v.getContext(), DetailCoworking.class);

                moveWithDataIntent.putExtra("Object", currentCoworking);
                v.getContext().startActivity(moveWithDataIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {

        private TextView name;
        private TextView address;
        private ImageView image;

        public ListViewHolder(@NonNull View itemView) {
            super(itemView);

//            coworkingName      = itemView.findViewById(R.id.co_name);
//            coworkingId   = itemView.findViewById(R.id.co_address);
//            date = itemView.findViewById(R.id.datePicker)
//            startAt

        }
    }
}
