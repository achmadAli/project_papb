package com.example.co_living.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.co_living.Model.Order;
import com.example.co_living.R;

import java.util.ArrayList;
import java.util.zip.Inflater;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.viewHolder> {
  private ArrayList<Order> orders;
  private Context context;

  public OrderAdapter(ArrayList<Order> orders, Context context) {
    this.orders = orders;
    this.context = context;
  }

  @NonNull
  @Override
  public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    return new viewHolder(LayoutInflater.from(context).inflate(R.layout.order_card, parent, false));
  }

  @Override
  public void onBindViewHolder(@NonNull viewHolder holder, int position) {
    Order current = orders.get(position);

    holder.namaCoworking.setText(current.getCoworkingName());
    holder.tanggal.setText(current.getDate());
    holder.durasi.setText(current.getDuration());
  }

  @Override
  public int getItemCount() {
    return orders.size();
  }

  public class viewHolder extends RecyclerView.ViewHolder {
    private TextView namaCoworking;
    private TextView tanggal;
    private TextView durasi;

    public viewHolder(@NonNull View itemView) {
      super(itemView);

      namaCoworking = itemView.findViewById(R.id.coworkingName);
      tanggal = itemView.findViewById(R.id.datePesan);
      durasi = itemView.findViewById(R.id.durasi);
    }
  }
}
