package com.example.co_living.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.co_living.DetailCoworking;
import com.example.co_living.ListCoworking;
import com.example.co_living.Model.Coworking;
import com.example.co_living.R;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class ListCoworkingAdapter extends RecyclerView.Adapter<ListCoworkingAdapter.ListViewHolder> {
  private ArrayList<Coworking> coworkings;
  private Context context;
  private String mode;

  public ListCoworkingAdapter(Context context, String mode, ArrayList<Coworking> coworkings) {
    this.coworkings = coworkings;
    this.context    = context;
    this.mode       = mode;
  }



  @NonNull
  @Override
  public ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    if(mode.equalsIgnoreCase("HORIZONTAL"))
      return new ListViewHolder(LayoutInflater.from(context).inflate(R.layout.coworking_card, parent, false));
    else
      return new ListViewHolder(LayoutInflater.from(context).inflate(R.layout.coworking_card_fullwidth, parent, false));

  }

  @Override
  public void onBindViewHolder(@NonNull final ListViewHolder holder, int position) {
    final Coworking currentCoworking = coworkings.get(position);

    holder.name.setText(currentCoworking.getName());
    holder.address.setText(currentCoworking.getAddress());
    Picasso.get().load(currentCoworking.getImg_path()).into(holder.image);

    holder.image.setOnClickListener(new View.OnClickListener(){
      @Override
      public void onClick(View v) {

        Intent moveWithDataIntent = new Intent(v.getContext(), DetailCoworking.class);

        moveWithDataIntent.putExtra("Object", currentCoworking);
        v.getContext().startActivity(moveWithDataIntent);
      }
    });
  }

  @Override
  public int getItemCount() {
    return coworkings.size();
  }

  public class ListViewHolder extends RecyclerView.ViewHolder {

    private TextView name;
    private TextView address;
    private ImageView image;

    public ListViewHolder(@NonNull View itemView) {
      super(itemView);

      name      = itemView.findViewById(R.id.co_name);
      address   = itemView.findViewById(R.id.co_address);
      image     = itemView.findViewById(R.id.co_image);

    }
  }


}
