package com.example.co_living.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;


import com.example.co_living.ListCoworking;
import com.example.co_living.Model.City;
import com.example.co_living.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.EventListener;

public class ListCityAdapter extends RecyclerView.Adapter<ListCityAdapter.ListViewHolder> {
  private ArrayList<City> listCity;
  private Context context;

  public ListCityAdapter(Context context, ArrayList<City> city) {
    this.context = context;
    this.listCity = city;
  }

  @Override
  public ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    // Create view from layout
    View itemView = LayoutInflater.from(context).inflate(R.layout.city_card,parent,false);
    return new ListViewHolder(itemView, this);
  }

  @Override
  public void onBindViewHolder(@NonNull final ListViewHolder holder, final int position) {
    final City current = listCity.get(position);
    // Add the data to the view
    holder.namaItemView.setText(current.getName());
    Picasso.get().load(current.getImage()).into(holder.imageView);

    holder.cardObj.setOnClickListener( new View.OnClickListener(){

      @Override
      public void onClick(View view) {
        Intent listCoworkingIntent = new Intent(view.getContext(), ListCoworking.class);

        listCoworkingIntent.putExtra("location", current.getName());
        view.getContext().startActivity(listCoworkingIntent);
      }
    });
  }

  @Override
  public int getItemCount() {
    return listCity.size();
  }

  public class ListViewHolder extends RecyclerView.ViewHolder {
    TextView namaItemView;
    ImageView imageView;
    CardView cardObj;

    public ListViewHolder(@NonNull View itemView, ListCityAdapter adapter) {
      super(itemView);
      // Get the layout
      namaItemView  = itemView.findViewById(R.id.city_name);
      imageView     = itemView.findViewById(R.id.cityImage);
      cardObj       = itemView.findViewById(R.id.city_card);
    }
  }

}
